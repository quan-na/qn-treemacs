#!/usr/bin/env fish
cp ../treemacs/src/elisp/*.el ./
cp ../treemacs/src/extra/*.el ./
cp ../treemacs/icons/default/*.png ./icons/default/

